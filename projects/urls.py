from django.urls import path
from projects.views import list_projects, task_detail, create_project

urlpatterns = [
    path("", list_projects, name="list_projects"),
    path("<int:id>/", task_detail, name="show_project"),
    path("create/", create_project, name="create_project"),
]
